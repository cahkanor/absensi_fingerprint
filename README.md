## DEPLOYMENT ##

1. Install paket yang dibutuhkan

sudo apt-get install python-dev redis-server supervisor python-pip git


2. Clone project

git clone https://adisatya_consultant@bitbucket.org/cahkanor/absensi_fingerprint.git


3. Masuk folder utama project dan install required package

cd absensi_fingerprint

sudo pip install -r requirement.txt

./manage.py collecstatic

4. Buat Database absensi_fingerprint untuk user eperformance dengan password tertentu

-  Masuk mysql console

mysql -u root -p

- Buat user eperformance

CREATE USER 'eperformance'@'localhost' IDENTIFIED BY 'TahuTakwa';

- Buat database absensi_fingerprint

create database absensi_fingerprint;

- Beri akses untuk user eperformance

grant usage on *.* to eperformance@localhost identified by 'TahuTakwa';

grant all privileges on absensi_fingerprint.* to eperformance@localhost;



5. Setting gunicorn lewat supervisor dengan menambahkan konfigurasi berikut pada /etc/supervisor/conf.d/fingerprint.conf

[program:fingerprint]
command = gunicorn absensi_fingerprint.wsgi
directory = /path/to/project/absensi_fingerprint
user = your_username

6. Setting periodic task untuk crawling dari mesin fingerprint dan mengirim ke server pusat dengan menambahkan konfigurasi berikut pada /etc/supervisor/conf.d/fingerprint.conf

[program:crawler]
command = python manage.py celery --app=absensi_fingerprint.celery:app worker -B
directory = /path/to/project/absensi_fingerprint
user = your_username

Atau untuk debugging, jalankan perintah berikut pada direktori utama absensi_fingerprint

python manage.py celery --app=absensi_fingerprint.celery:app worker --loglevel=ERROR --logfile=/home/cominfo/celery.log -B

NB. : log level dapat diganti INFO

7. Lakukan reread dan update supervisor

```
#!bash

sudo service supervisor start
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start [nama_job]
```






