from django.contrib import admin, messages
from display_absensi.models import Pegawai, Setting, Fingerprint, FingerprintLog
from display_absensi.utils import get_setting, AESdecrypt
from display_absensi.views import send_log, save_log, sinkronisasi_mesin, sinkronisasi_server, sinkronisasi_pegawai
import drest
import httplib
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django import forms
from django.db import models
import pytz
from datetime import timedelta
from django.utils.safestring import mark_safe
from xml.etree import ElementTree
# Register your models here.
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import user_passes_test

class holder(object):
	success = False #Default proses dianggap gagal
	pesan = ''

def sinkron_pegawai(obj_, request, params_ = dict()):
		sett = get_setting()
		try :
			try :
				api = drest.api.TastyPieAPI(sett.api_url)
				api.auth(sett.api_user_name, sett.api_key)
				data = api.pegawai.get(params=params_)
				if data.status == 200 :
					try :
						obj_data = data.data['objects']
						for i in obj_data:
							p, created = Pegawai.objects.get_or_create(id_fingerprint=i['id'])
							p.nip = i['nomor_identitas']
							p.nama = i['nama_lengkap']
							p.gelar_depan = i['gelar_depan']
							p.gelar_belakang = i['gelar_belakang']
							if i['jabatan']:
								p.jabatan = str(i['jabatan']).replace('<br />', '')
							p.foto = i['foto']
							# if created :
							# 	p.id_fingerprint = i['id']
							p.save()
						msg = 'Sinkronisasi berhasil.'
						obj_.message_user(request, msg, messages.SUCCESS)
					except KeyError :
						msg = 'Sinkronisasi gagal, data yg diterima tidak sesuai.'
						obj_.message_user(request, msg, messages.ERROR)
				else:
					msg = 'Koneksi server error, coba beberapa saat kemudian.'
					obj_.message_user(request, msg, messages.ERROR)
			except drest.exc.dRestAPIError :
				msg = 'Koneksi server error, coba beberapa saat kemudian.'
				obj_.message_user(request, msg, messages.ERROR)
		except drest.exc.dRestRequestError :
			msg = 'Koneksi server error, coba beberapa saat kemudian.'
			obj_.message_user(request, msg, messages.ERROR)

def send_command(pegawai_admin, request, command, ip_fingerprint) :
	resp = holder()
	try :
		connection = httplib.HTTPConnection(ip_fingerprint)
		connection.putrequest('POST', '/iWsService')
		connection.putheader('Content-Type', 'text/xml')
		connection.putheader('Content-Length', str(len(command)))
		connection.endheaders()
		connection.send(command)
		resp.success = True
		resp.pesan = connection.getresponse().read()
	except Exception :
		resp.pesan = "Tidak dapat konek ke Mesin Fingerprint "+ip_fingerprint+", Coba periksa settingan alamat fingerprint apakah bisa terhubung ke Raspberry Pi Server."
	return resp		
	### Nanti kirim data ke server terkait error ini pada fingerprint ini

def refresh_db(pegawai_admin, request, ip_fingerprint, key_fingerprint):
	resp = holder()
	try :	
		command = '<RefreshDB>\r\n'+'<ArgComKey xsi:type="xsd:integer">'+str(key_fingerprint)+'</ArgComKey>\r\n'+'</RefreshDB>\r\n'+'\r\n'
		resp = send_command(pegawai_admin, request, command, ip_fingerprint)
	except Exception :
		resp.pesan = "Tidak dapat konek ke Mesin Fingerprint "+ip_fingerprint+", Coba periksa settingan alamat fingerprint apakah bisa terhubung ke Raspberry Pi Server."
	return resp

class PegawaiAdmin(admin.ModelAdmin):
	list_display = ('id_fingerprint', 'nama', 'nip', 'jabatan', 'pin_password', 'jumlah_jari', 'admin_fp', "pegawai_setting", 'sinkron_mesin', 'sinkron_server')
	ordering = ('id_fingerprint',)
	readonly_fields = ( "pin", "privilege", "foto", "id_fingerprint", "jabatan", "setting", "nip","nama","gelar_depan","gelar_belakang",)
	filter_horizontal = ('setting',)
	list_display_links = ('nama',)

	def pin_password(self, obj):
		if obj.pin:
			if obj.pin != "None" and obj.pin != "" and obj.pin != '0':
				set_ = obj.setting.all()
				if set_.exists():
					sett = set_.last()
					return AESdecrypt(str(sett.get_api_key()), str(obj.pin))
		return "-"
	pin_password.short_description = 'PIN'

	def admin_fp(self, obj):
		if obj.privilege:
			if obj.privilege == 14:
				return 'Admin'
			else:
				return 'Bukan'
		else:
			return "-"
	admin_fp.short_description = 'Admin Mesin'

	def jumlah_jari(self, obj):
		return obj.fingerprinttemplate_set.all().count()
	jumlah_jari.short_description = 'Jumlah Jari'

	def pegawai_setting(self, obj):
		set_ = obj.setting.all()
		if set_.count() < 5 and set_.count() > 0:
			return ", ".join(str(s.ip_fingerprint) for s in set_)
		else:
			return str(obj.setting.count())+" Setting"
	pegawai_setting.short_description = 'Setting'

	def has_add_permission(self, request):
		return False	

	@method_decorator(user_passes_test(lambda u: u.is_superuser))
	def sync_mesin(self, request):		
		sinkronisasi_mesin()
		redirect_url = reverse("admin:display_absensi_pegawai_changelist")
		return HttpResponseRedirect(redirect_url)

	@method_decorator(user_passes_test(lambda u: u.is_superuser))
	def sync_server(self, request):	
		sinkronisasi_server()
		redirect_url = reverse("admin:display_absensi_pegawai_changelist")
		return HttpResponseRedirect(redirect_url)
	def sync_pegawai(self, request):	
		sinkronisasi_pegawai()
		redirect_url = reverse("admin:display_absensi_pegawai_changelist")
		return HttpResponseRedirect(redirect_url)

	def sinkronisasi_pegawai_by_skpd(self, request, skpd=None):
		get_params = dict(skpd = skpd, limit=0,)
		sinkron_pegawai(self, request, get_params)
		redirect_url = reverse("admin:display_absensi_pegawai_changelist")
		return HttpResponseRedirect(redirect_url)

	def sinkronisasi_fingerprint(self, request, fp=None):
		get_params = dict(fingerprint = fp, limit=0,)
		sinkron_pegawai(self, request, get_params)
		redirect_url = reverse("admin:display_absensi_pegawai_changelist")
		return HttpResponseRedirect(redirect_url)

	def sinkronisasi_pegawai_by_bidang(self, request, skpd=None, bidang=None):
		get_params = dict(bidang = bidang, limit=0,)
		sinkron_pegawai(self, request, get_params)
		redirect_url = reverse("admin:display_absensi_pegawai_changelist")
		return HttpResponseRedirect(redirect_url)

	def sinkronisasi_pegawai_by_wilayah(self, request, skpd=None, bidang=None, wilayah=None):
		if not skpd or skpd == "0":
			get_params = dict(wilayah_kerja = wilayah, limit=0,)
		else:
			get_params = dict(skpd = skpd, wilayah_kerja = wilayah, limit=0,)
		sinkron_pegawai(self, request, get_params)
		redirect_url = reverse("admin:display_absensi_pegawai_changelist")
		return HttpResponseRedirect(redirect_url)

	def get_fp(self, request):
		FP_CHOICES = list()
		sett = get_setting()
		if sett is None:
			return HttpResponse("Alamat Server tidak diketahui, Pastikan Anda sudah menyetting alamat server dengan benar.");
		try :
			try :		
				api = drest.api.TastyPieAPI(sett.api_url)
				api.auth(sett.api_user_name, sett.api_key)
				get_params = dict(limit=0,)
				data = api.fp.get(params=get_params)		
				if data.status == 200 :
					try :
						obj_data = data.data['objects']
						for i in obj_data:
							FP_CHOICES.append((i['id'], i['nama_lengkap'], i['nomor_identitas'],))
					except KeyError :
						return HttpResponse("Pengambilan data gagal, data yg diterima tidak sesuai..");
				else:
					return HttpResponse("Koneksi server error, coba beberapa saat kemudian.");
			except drest.exc.dRestAPIError :
				return HttpResponse("Koneksi server error, coba beberapa saat kemudian.");
		except drest.exc.dRestRequestError :
			return HttpResponse("Koneksi server error, coba beberapa saat kemudian.");
		pilihan = "<option></option>"
		for s in FP_CHOICES:
			pilihan += "<option value='"+str(s[2])+"'>"+str(s[1])+"</option>"
		return HttpResponse(mark_safe(pilihan));

	def get_skpd(self, request):
		SKPD_CHOICES = list()
		sett = get_setting()
		if sett is None:
			return HttpResponse("Alamat Server tidak diketahui, Pastikan Anda sudah menyetting alamat server dengan benar.");
		try :
			try :		
				api = drest.api.TastyPieAPI(sett.api_url)
				get_params = dict(limit=0,)
				data = api.skpd.get(params=get_params)

				if data.status == 200 :
					try :
						obj_data = data.data['objects']
						# return HttpResponse(obj_data);
						for i in obj_data:
							SKPD_CHOICES.append((i['id'], i['nama_skpd'], i['level'], i['jumlah']))
					except KeyError :
						return HttpResponse("Pengambilan data gagal, data yg diterima tidak sesuai..");
				else:
					return HttpResponse("Koneksi server error, coba beberapa saat kemudian.");
			except drest.exc.dRestAPIError :
				return HttpResponse("Koneksi server error, coba beberapa saat kemudian.");
		except drest.exc.dRestRequestError :
			return HttpResponse("Koneksi server error, coba beberapa saat kemudian.");
		pilihan = "<option></option>"
		strip = '---'
		for s in SKPD_CHOICES:
			pilihan += "<option value='"+str(s[0])+"'>"+str(s[2]*strip)+str(s[1])+" ("+str(s[3])+" Pegawai)</option>"
		return HttpResponse(mark_safe(pilihan));

	def get_bidang(self, request, skpd=None):
		BIDANG_CHOICES = list()
		sett = get_setting()

		try :
			try :		
				api = drest.api.TastyPieAPI(sett.api_url)
				api.auth(sett.api_user_name, sett.api_key)
				get_params = dict(skpd = skpd,limit=0,)
				data = api.bidang.get(params=get_params)
				if data.status == 200 :
					try :
						obj_data = data.data['objects']
						for i in obj_data:
							BIDANG_CHOICES.append((i['id'], i['nama_bidang'], i['level']))
					except KeyError :
						return HttpResponse("Pengambilan data gagal, data yg diterima tidak sesuai..");
				else:
					return HttpResponse("Koneksi server error, coba beberapa saat kemudian.");
			except drest.exc.dRestAPIError :
				return HttpResponse("Koneksi server error, coba beberapa saat kemudian.");
		except drest.exc.dRestRequestError :
			return HttpResponse("Koneksi server error, coba beberapa saat kemudian.");

		pilihan = "<option></option>"
		strip = '---'
		for s in BIDANG_CHOICES:
			pilihan += "<option value='"+str(s[0])+"'>"+str(s[2]*strip)+str(s[1])+"</option>"
		return HttpResponse(mark_safe(pilihan));

	def get_wilayah(self, request, skpd=None):
		WILAYAH_CHOICES = list()
		sett = get_setting()

		try :
			try :		
				api = drest.api.TastyPieAPI(sett.api_url)
				api.auth(sett.api_user_name, sett.api_key)
				get_params = dict(skpd = skpd,limit=0,)
				data = api.wilayah.get(params=get_params)
				if data.status == 200 :
					try :
						obj_data = data.data['objects']
						for i in obj_data:
							WILAYAH_CHOICES.append((i['id'], i['wilayah_kerja']))
					except KeyError :
						return HttpResponse("Pengambilan data gagal, data yg diterima tidak sesuai..");
				else:
					return HttpResponse("Koneksi server error, coba beberapa saat kemudian.");
			except drest.exc.dRestAPIError :
				return HttpResponse("Koneksi server error, coba beberapa saat kemudian.");
		except drest.exc.dRestRequestError :
			return HttpResponse("Koneksi server error, coba beberapa saat kemudian.");
		pilihan = "<option></option>"
		for s in WILAYAH_CHOICES:
			pilihan += "<option value='"+str(s[1])+"'>"+str(s[1])+"</option>"
		return HttpResponse(mark_safe(pilihan));

	def get_urls(self):
		from django.conf.urls import patterns, url

		urls = super(PegawaiAdmin, self).get_urls()
		my_urls = patterns('',
				url(r'^sync_mesin/$', self.admin_site.admin_view(self.sync_mesin), name='sync_mesin'),
				url(r'^sync_server/$', self.admin_site.admin_view(self.sync_server), name='sync_server'),
				url(r'^sync_pegawai/$', self.admin_site.admin_view(self.sync_pegawai), name='sync_pegawai'),
				url(r'^get_fp/$', self.admin_site.admin_view(self.get_fp), name='get_fp'),
				url(r'^get_skpd/$', self.admin_site.admin_view(self.get_skpd), name='get_skpd'),
				url(r'^get_bidang/(?P<skpd>\w+)/$', self.admin_site.admin_view(self.get_bidang), name='get_bidang'),
				url(r'^get_wilayah/(?P<skpd>\w+)/$', self.admin_site.admin_view(self.get_wilayah), name='get_wilayah'),
				url(r'^sinkronisasi_fingerprint/(?P<fp>\w+)/$', self.admin_site.admin_view(self.sinkronisasi_fingerprint), name='sinkronisasi_fingerprint'),
				url(r'^sinkronisasi_pegawai/(?P<skpd>\w+)/$', self.admin_site.admin_view(self.sinkronisasi_pegawai_by_skpd), name='sinkronisasi_pegawai_skpd'),
				url(r'^sinkronisasi_pegawai/(?P<skpd>\w+)/(?P<bidang>\w+)/$', self.admin_site.admin_view(self.sinkronisasi_pegawai_by_bidang), name='sinkronisasi_pegawai_bidang'),
				url(r'^sinkronisasi_pegawai/(?P<skpd>\w+)/(?P<bidang>\w+)/(?P<wilayah>.+)/$', self.admin_site.admin_view(self.sinkronisasi_pegawai_by_wilayah), name='sinkronisasi_pegawai_wilayah'),
		)
		return my_urls + urls

	def get_actions(self, request):
		actions = super(PegawaiAdmin, self).get_actions(request)
		if not request.user.is_superuser:
			if 'delete_selected' in actions:
				del actions['delete_selected']
		return actions

	def changelist_view(self, request, extra_context={}):       
		extra_context.update({'has_delete_permission' : self.has_delete_permission(request)})
		# sett = get_setting()
		# server_base_url = sett.api_url.split("/api/")
		# wilayah_url = str(server_base_url[0])+"/admin/kepegawaian/userpegawai/wilayah/"
		# extra_context.update({'wilayah_url' : wilayah_url})
		return super(PegawaiAdmin, self).changelist_view(request, extra_context=extra_context)

# from django.db import connection

class SettingForm(forms.ModelForm):	
	key_fingerprint = forms.CharField(label='Key Fingerprint', widget=forms.PasswordInput(render_value = True))
	api_key = forms.CharField(label='API Key', widget=forms.PasswordInput(render_value = True))

	class Meta:
		model = Setting
		fields = ['ip_fingerprint', 'key_fingerprint', 'aktif', 'api_url', 'api_user_name', 'api_key']

class SettingAdmin(admin.ModelAdmin):
	# form = SettingForm
	list_display = ('ip_fingerprint', 'api_url', 'aktif' )

	def save_model(self, request, obj, form, change):
		# if obj.aktif:
		# 	cursor = connection.cursor()
		# 	cursor.execute("UPDATE display_absensi_setting SET aktif = 0 WHERE aktif = 1")
		obj.save()
		# if obj.aktif and obj.otomatis:
		# 	get_params = dict(fingerprint = obj.api_user_name, limit=0,)
		# 	sinkron_pegawai(self, request, get_params)

	def get_actions(self, request):
		actions = super(SettingAdmin, self).get_actions(request)
		if not request.user.is_superuser:
			if 'delete_selected' in actions:
				del actions['delete_selected']
		return actions
		
	def get_form(self, request, obj=None, **kwargs):
		if request.user.is_superuser:
			kwargs['form'] = SettingForm
		return super(SettingAdmin, self).get_form(request, obj, **kwargs)

	def get_fieldsets(self, request, obj = None):
		if not request.user.is_superuser:
			fieldsets = (
				(None, {'fields': ('ip_fingerprint', 'api_url',)}),
			)
		else:
			fieldsets = (
				(None, {'fields': ('ip_fingerprint', 'key_fingerprint', 'aktif',)}),
				('API Server', {'fields': ('api_url', 'api_user_name', 'api_key')}),
			)
		return fieldsets
	def get_readonly_fields(self, request, obj=None):
		rf = ()
		if not request.user.is_superuser:
			rf = ('key_fingerprint', 'aktif', 'api_user_name', 'api_key')
		return rf
	def changelist_view(self, request, extra_context={}):        
		# self.message_user(request, "Anda cuman bisa mengaktifkan salah satu settingan.")
		return super(SettingAdmin, self).changelist_view(request, extra_context=extra_context)

class FingerprintAdmin(admin.ModelAdmin):
	list_display = ('setting', 'id_fingerprint', 'jenis_waktu', 'tipe_waktu', 'terkirim' )
	# actions = None
	readonly_fields = ('id_fingerprint', 'waktu', 'tipe_waktu', 'pegawai', 'terkirim', 'setting')

	def jenis_waktu(self, obj):
		return obj.waktu.replace(tzinfo=pytz.timezone("Asia/Jakarta"))+timedelta(minutes=7)
	jenis_waktu.short_description = 'Waktu'
	jenis_waktu.admin_order_field = 'waktu'
	
	def has_add_permission(self, request, obj=None):
		return False

	# def has_delete_permission(self, request, obj=None):
	# 	return False

	def __init__(self, *args, **kwargs):
		super(FingerprintAdmin, self).__init__(*args, **kwargs)
		self.list_display_links = (None, )

class FingerprintLogAdmin(admin.ModelAdmin):
	list_display = ('setting', 'status', 'kategori', 'log', 'terkirim', 'created_at', 'updated_at' )
	# actions = None
	readonly_fields = ('status', 'kategori', 'log', 'terkirim', 'created_at', 'updated_at', 'setting')
	
	def has_add_permission(self, request, obj=None):
		return False

	# def has_delete_permission(self, request, obj=None):
	# 	return False

	def __init__(self, *args, **kwargs):
		super(FingerprintLogAdmin, self).__init__(*args, **kwargs)
		self.list_display_links = (None, )

admin.site.register(Pegawai, PegawaiAdmin)
admin.site.register(Setting, SettingAdmin)
admin.site.register(Fingerprint, FingerprintAdmin)
admin.site.register(FingerprintLog, FingerprintLogAdmin)
