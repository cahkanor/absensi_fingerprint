from django.apps import AppConfig

class DisplayConfig(AppConfig):
    name = 'display_absensi'
    verbose_name = "Display Absensi"