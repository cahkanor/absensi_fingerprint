import babel.dates
import os
import subprocess
import drest
from django.template.loader import render_to_string
from django.core import serializers
import urllib2
# from bs4 import BeautifulSoup
from xml.etree import ElementTree
from django.templatetags.static import static
from django.shortcuts import render, render_to_response
from django.template import RequestContext, loader
from django.http import HttpResponse, HttpResponseRedirect
from display_absensi.models import Pegawai, Fingerprint, Setting, FingerprintLog, FingerprintTemplate
import datetime, calendar
import pytz
# from datetime import timedelta
import decimal
import requests
import commands
import httplib
from display_absensi.utils import get_setting, mycmp, get_version, set_version, encrypt_file, AESencrypt, AESdecrypt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import netifaces as ni
from dateutil.relativedelta import relativedelta
from django.utils.safestring import mark_safe

class holder(object):
	success = False #Default proses dianggap gagal
	pesan = ''

def save_log(status_, kategori_, log_, sett_):
	fp_log = FingerprintLog(status = status_, kategori = kategori_, log = log_, setting= sett_)
	fp_log.save()
	return fp_log

# Create your views here.
def send_log(status_, kategori_, log_, sett_, waktu_, save_):
	resp = holder()
	try :
		try :
			try :
				if not waktu_:
					now = datetime.datetime.now()
					waktu_ = now.strftime('%Y-%m-%d %H:%M:%S')
				api = drest.api.TastyPieAPI(sett_.api_url)
				api.auth(sett_.api_user_name, sett_.api_key)
				api.request.add_header('X-CSRFToken', '{{csrf_token}}')
				api.request.add_header('Content-Type','application/json')
				log_data = dict(status = status_, kategori = kategori_, log = log_, fingerprint=sett_.api_user_name, created_at = waktu_)
				response = api.log.post(log_data)
				if response.status in (200, 201, 202) :
					resp.pesan = "[X300]["+str(response.status)+"] Data log "+str(kategori_)+" "+str(log_)+" BERHASIL dikirim ke server dengan ID Fingerprint "+str(sett_.api_user_name)+"."
					resp.success = True
				else :
					resp.pesan = "[X200]["+str(response.status)+"] Data log "+str(kategori_)+" "+str(log_)+" GAGAL dikirim ke server dengan ID Fingerprint "+str(sett_.api_user_name)+"."
			except drest.exc.dRestAPIError :
				resp.pesan = '[X100][1]. Koneksi server error.'
		except drest.exc.dRestRequestError :
			resp.pesan = '[X100][2]. Koneksi server error.'
	except Exception :
		resp.pesan = '[X100][3]. Koneksi server error.'
	if not resp.success and save_:
		save_log(status_, kategori_, log_, sett_)
	return resp

def get_server_time(sett_):
	server_base_url = sett_.api_url.split("/api/")[0]+"/server_datetime_2/"
	req = None
	
	try:
		req = requests.get(server_base_url)
	except Exception as e:
		req = None
		save_log('Error', 'get_server_time', '[X100]. Tidak dapat konek ke server.', sett_)
	return req

# menghapus log sebelum seminggu terakhir 
def hapus_log():
	#Fingerprint.objects.exclude(waktu__startswith=now.date()).delete()
	now = datetime.datetime.now()
	minggu_lalu = now - datetime.timedelta(days=6)
	fp_ = FingerprintLog.objects.exclude(created_at__gt=minggu_lalu)
	fp_.exclude(terkirim=False).delete()

	list_log = FingerprintLog.objects.filter(terkirim=False)
	for l in list_log:
		resp_send_log = send_log(l.status, l.kategori, l.log, l.setting, l.created_at.astimezone(pytz.timezone("Asia/Jakarta")).strftime('%Y-%m-%d %H:%M:%S'), False)
		if resp_send_log.success:
			l.delete()
	return "Hapus Log"

def send_command(command, sett_):
	resp = holder()
	try :
		connection = httplib.HTTPConnection(sett_.ip_fingerprint, timeout=60)
		connection.putrequest('POST', '/iWsService')
		connection.putheader('Content-Type', 'text/xml')
		connection.putheader('Content-Length', str(len(command)))
		connection.endheaders()
		connection.send(command)
		resp.success = True
		resp.pesan = connection.getresponse().read()
	except Exception :
		resp.pesan = "[X101]. Tidak dapat konek ke Mesin Fingerprint "+str(sett_.ip_fingerprint)+", Coba periksa settingan alamat fingerprint apakah bisa terhubung ke Raspberry Pi Server."
	return resp

def restart_mesin(sett_):
	resp = holder()
	try :	
		command = '<?xml version="1.0" encoding="UTF-8" standalone="no"?><SOAP-ENV:Envelope SOAP-ENV:encodingStyle="" xmlns:SOAPSDK1="http://www.w3.org/2001/XMLSchema" xmlns:SOAPSDK2="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAPSDK3="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Body SOAP-ENV:encodingStyle=""><SOAPSDK4:Restart xmlns:SOAPSDK4="http://www.zksoftware/Service/message/" SOAP-ENV:encodingStyle=""><SOAPSDK4:ArgComKey SOAP-ENV:encodingStyle="">'+str(sett_.key_fingerprint)+'</SOAPSDK4:ArgComKey></SOAPSDK4:Restart></SOAP-ENV:Body></SOAP-ENV:Envelope>'
		resp = send_command(command, sett_)
	except Exception :
		resp_send_log = send_log('Error', 'restart_mesin', '[X101]. Tidak dapat konek ke mesin fingerprint.', sett_, None, True)
	return resp

def refresh_db(sett_):
	resp = holder()
	try :	
		command = '<RefreshDB>\r\n'+'<ArgComKey xsi:type="xsd:integer">'+str(sett_.key_fingerprint)+'</ArgComKey>\r\n'+'</RefreshDB>\r\n'+'\r\n'
		resp = send_command(command, sett_)
	except Exception :
		resp_send_log = send_log('Error', 'refresh_db', '[X101]. Tidak dapat konek ke mesin fingerprint.', sett_, None, True)
	return resp

def sinkronisasi_jari(pegawai_, sett_):
	resp = holder()
	jari_list = pegawai_.fingerprinttemplate_set.all()
	for jari in jari_list:
		try :
			try :
				try :
					api = drest.api.TastyPieAPI(sett_.api_url)
					api.auth(sett_.api_user_name, sett_.api_key)
					api.request.add_header('X-CSRFToken', '{{csrf_token}}')
					api.request.add_header('Content-Type','application/json')
					data_jari = dict(pegawai=pegawai_.id_fingerprint, action="sinkron", fingerprint_id= jari.fingerprint_id, sz = jari.size, vld=jari.valid, tmplt=jari.template)
					response = api.jari.get(params=data_jari)
					if response.status in (200, 201, 202) :
						try :
							obj_data = response.data['objects']
							# print obj_data
							for i in obj_data:
								f, created = FingerprintTemplate.objects.update_or_create(pegawai=pegawai_, fingerprint_id=i['fingerprint_id'])
								f.size = i['size']
								f.valid = i['valid']
								f_template = i['template']
								if f_template and f_template != '' and f_template != 'None':
									f.template = f_template
								f.sinkron_server = True
								f.save()
							resp.pesan = "[X300]["+str(response.status)+"] Data jari "+str(pegawai_)+" "+str(jari.fingerprint_id)+" "+str(jari.size)+" BERHASIL dikirim ke server dengan ID Fingerprint "+str(sett_.api_user_name)+"."
							resp.success = True
						except KeyError :
							resp.pesan = '[X200]. Sinkronisasi gagal, data yg diterima tidak sesuai.'
					else :
						resp.pesan = "[X200]["+str(response.status)+"] Data jari "+str(pegawai_)+" "+str(jari.fingerprint_id)+" "+str(jari.size)+" GAGAL dikirim ke server dengan ID Fingerprint "+str(sett_.api_user_name)+"."
				except drest.exc.dRestAPIError :
					resp.pesan = '[X100][1]. Koneksi server error, coba beberapa saat kemudian.'
			except drest.exc.dRestRequestError :
				resp.pesan = '[X100][2]. Koneksi server error, coba beberapa saat kemudian.'
		except Exception :
			resp.pesan = '[X100][3]. Koneksi server error, coba beberapa saat kemudian.'
	return resp

def sinkronisasi_server():
	resp = holder()
	pegawai_list = Pegawai.objects.all()
	
	for pegawai in pegawai_list:
		# Sinkron Data Pegawai dilakukan jika sinkronisasi mesin selesai
		print pegawai
		if pegawai.sinkron_mesin and not pegawai.sinkron_server:

			sett_list = pegawai.setting.all()
			for sett in sett_list:
				try :
					try :
						try :
							api = drest.api.TastyPieAPI(sett.api_url)
							api.auth(sett.api_user_name, sett.api_key)
							get_params = dict(id=pegawai.id_fingerprint, action="sinkron", pn=pegawai.pin, pv=pegawai.privilege, af=sett.api_user_name, limit=0,)
							response = api.pegawai.get(params=get_params)
							
							if response.status == 200 :
								try :
									obj_data = response.data['objects']
									# print obj_data
									for i in obj_data:
										p, created = Pegawai.objects.get_or_create(id_fingerprint=i['id'])
										p.nip = i['nomor_identitas']
										p.nama = i['nama_lengkap']
										p.pin = i['pin']
										p.privilege = i['privilege']
										p.gelar_depan = i['gelar_depan']
										p.gelar_belakang = i['gelar_belakang']
										if i['jabatan']:
											p.jabatan = str(i['jabatan']).replace('<br />', '')
										p.foto = i['foto']
										if created:
											p.setting.add(sett)
										elif p.setting:
											if not p.setting.all().filter(id=sett.id).exists():
												p.setting.add(sett)
										p.save()
										print p
									resp.pesan = '[X200]. Sinkronisasi pegawai berhasil.'
									resp.success = True
								except KeyError :
									resp.pesan = '[X200]. Sinkronisasi gagal, data yg diterima tidak sesuai.'
							else:
								resp.pesan = '[X200]. Koneksi server error, coba beberapa saat kemudian.'
						except drest.exc.dRestAPIError :
							resp.pesan = '[X100][1]. Koneksi server error, coba beberapa saat kemudian.'
					except drest.exc.dRestRequestError :
						resp.pesan = '[X100][2]. Koneksi server error, coba beberapa saat kemudian.'
				except Exception :
					resp.pesan = '[X100][3]. Koneksi server error, coba beberapa saat kemudian.'
			# Sinkron Jari Pegawai
			if resp.success:
				sett_ = sett_list.last()
				respon_jari = sinkronisasi_jari(pegawai, sett_)
				if respon_jari.success and resp.success:
					if not pegawai.fingerprinttemplate_set.all().filter(sinkron_server=False).exists():
						pegawai.sinkron_server=True
						pegawai.save()
					elif pegawai.fingerprinttemplate_set.all().filter(sinkron_server=False).exists():
						pegawai.sinkron_server=False
						pegawai.save()
				elif resp.success and not respon_jari.success:
					if pegawai.fingerprinttemplate_set.all().count() == 0:
						pegawai.sinkron_server=True
						pegawai.save()
					elif pegawai.fingerprinttemplate_set.all().count() > 0:
						pegawai.sinkron_server=False
						pegawai.save()
	return resp

# Untuk sinkronisasi data mesin (Semua data yg ada di mesin ada di raspberry dan siabjo)
def set_userinfo_mesin(sett_, data_, sinkronisasi_):
	resp = holder()
	command = '<SetUserInfo>\r\n'+'<ArgComKey xsi:type="xsd:integer">'+str(sett_.key_fingerprint)+'</ArgComKey>\r\n'
	for i in data_:
		if not sinkronisasi_ or sinkronisasi_ and not i.sinkron_mesin:
			nama = ''
			if i.nama:
				nama = i.nama.replace("'"," ").replace("`", " ")
				
			privilege = ''
			if i.privilege:
				privilege = i.privilege

			pin = ''
			if i.pin and i.pin != "None" and i.pin != "" and i.pin != "0":
				pin = AESdecrypt(str(sett_.get_api_key()), str(i.pin))

			command = command + '<Arg><PIN xsi:type="xsd:integer">'+str(i.id_fingerprint)+'</PIN><Name xsi:type="xsd:integer">'+nama+'</Name><Privilege xsi:type="xsd:integer">'+str(privilege)+'</Privilege><Password xsi:type="xsd:integer">'+str(pin)+'</Password></Arg>\r\n'
	command = command +'</SetUserInfo>\r\n'+'\r\n'
	resp = send_command(command, sett_)
	return resp

# Untuk sinkronisasi data mesin (Semua data yg ada di mesin ada di raspberry dan siabjo)
def sinkronisasi_mesin():
	sett_list = get_setting()
	resp = holder()
	for sett in sett_list:
		data = sett.pegawai_set.all()

		resp = set_userinfo_mesin(sett, data, True)

		if resp.success:
			refresh_db(sett)
			# refresh_db(self, request, sett.ip_fingerprint, sett.key_fingerprint)
			command = '<GetUserInfo>\r\n'+'<ArgComKey xsi:type="xsd:integer">'+str(sett.key_fingerprint)+'</ArgComKey>\r\n'
			command = command + '<Arg><PIN xsi:type="xsd:integer">All</PIN></Arg>\r\n'
			command = command +'</GetUserInfo>\r\n'+'\r\n'
			respon = send_command(command, sett)
			# respon = send_command(self, request, command, sett.ip_fingerprint)
			if respon.success:
				tree = ElementTree.fromstring(respon.pesan)
				data_user_info = tree.findall("Row")
				for fp_user_info in data_user_info:
					f_id = fp_user_info[6].text
					f_nama = fp_user_info[1].text
					f_pin = fp_user_info[2].text
					f_privilege = fp_user_info[4].text
					
					p, created = Pegawai.objects.get_or_create(id_fingerprint=f_id)
					
					if created:
						p.nama = f_nama
						p.setting.add(sett)
					elif p.setting:
						if not p.setting.all().filter(id=sett.id).exists():
							p.setting.add(sett)
					if not p.nama:
						p.nama = f_nama
					elif p.nama == '':
						p.nama = f_nama
					if f_pin and f_pin != '':
						p.pin = AESencrypt(str(sett.get_api_key()), str(f_pin))
					p.privilege = f_privilege
					p.sinkron_mesin = True
					p.save()
					print p

				command = '<GetUserTemplate>\r\n'+'<ArgComKey xsi:type="xsd:integer">'+str(sett.key_fingerprint)+'</ArgComKey>\r\n'
				command = command + '<Arg><PIN xsi:type="xsd:integer">All</PIN></Arg>\r\n'
				command = command +'</GetUserTemplate>\r\n'+'\r\n'
				respon_template = send_command(command, sett)
				# respon = send_command(self, request, command, sett.ip_fingerprint)
				if respon_template.success:
					tree = ElementTree.fromstring(respon_template.pesan)
					data_user_template = tree.findall("Row")
					for fp_user_template in data_user_template:
						f_pin = fp_user_template[0].text
						f_id = fp_user_template[1].text
						f_size = fp_user_template[2].text
						f_valid = fp_user_template[3].text
						f_template = fp_user_template[4].text
						pegawai = Pegawai.objects.filter(id_fingerprint=f_pin)
						if pegawai.exists():
							f, created = FingerprintTemplate.objects.update_or_create(pegawai=pegawai.last(), fingerprint_id=f_id)
							f.size = f_size
							f.valid = f_valid
							if f_template and f_template != '':
								f.template = AESencrypt(str(sett.get_api_key()), str(f_template))
							f.sinkron_mesin = True
							f.save()
							print pegawai
					# sinkronisasi_server()
		else:
			resp_send_log = send_log('Error', 'sync_mesin', resp.pesan, sett, None, True)
			# Penyesuaian data mesin dengan siabjo
def set_jari_mesin(pegawai_, sett_):
	resp = holder()
	jari_list = pegawai_.fingerprinttemplate_set.all()
	command = '<SetUserTemplate>\r\n'+'<ArgComKey xsi:type="xsd:integer">'+str(sett_.key_fingerprint)+'</ArgComKey>\r\n'
	for jari in jari_list:
		if jari.sinkron_server and jari.sinkron_mesin:
			template = ''
			if jari.template and jari.template != "None" and jari.template != "":
				template = AESdecrypt(str(sett_.get_api_key()), str(jari.template))
			command = command + '<Arg><PIN xsi:type="xsd:integer">'+str(jari.pegawai.id_fingerprint)+'</PIN><FingerID xsi:type="xsd:integer">'+str(jari.fingerprint_id)+'</FingerID><Size xsi:type="xsd:integer">'+str(jari.size)+'</Size><Valid xsi:type="xsd:integer">'+str(jari.valid)+'</Valid><Template xsi:type="xsd:integer">'+str(template)+'</Template></Arg>\r\n'
	command = command +'</SetUserTemplate>\r\n'+'\r\n'
	resp = send_command(command, sett_)
	return resp

def sinkronisasi_jari_pegawai(pegawai_, sett_):
	resp = holder()
	# jari_list = pegawai_.fingerprinttemplate_set.all()
	# for jari in jari_list:
	try :
		try :
			try :
				api = drest.api.TastyPieAPI(sett_.api_url)
				api.auth(sett_.api_user_name, sett_.api_key)
				api.request.add_header('X-CSRFToken', '{{csrf_token}}')
				api.request.add_header('Content-Type','application/json')
				data_jari = dict(pegawai=pegawai_.id_fingerprint)
				response = api.jari.get(params=data_jari)
				if response.status in (200, 201, 202) :
					try :
						obj_data = response.data['objects']
						# print obj_data
						baru_ = False
						for i in obj_data:
							f, created = FingerprintTemplate.objects.update_or_create(pegawai=pegawai_, fingerprint_id=i['fingerprint_id'])
							f.size = i['size']
							f.valid = i['valid']
							f_template = i['template']
							if f_template and f_template != '' and f_template != 'None':
								f.template = f_template
							f.sinkron_server = True
							f.sinkron_mesin = True
							f.save()
							if created:
								baru_ = True
						if baru_:
							resp.pesan = "[X300]["+str(response.status)+"] Data jari "+str(pegawai_)+" baru ditambahkan di raspberry "+str(sett_.api_user_name)+"."
							save_log('Warning', 'sinkronisasi_jari_pegawai', resp.pesan, sett_)
						# else:
						# 	resp.pesan = "[X300]["+str(response.status)+"] Data jari "+str(pegawai_)+" di raspberry "+str(sett_.api_user_name)+" telah diperbaruhi."
						resp.success = True
					except KeyError :
						resp.pesan = '[X200]Sinkronisasi gagal, data yg diterima tidak sesuai.'
				else :
					resp.pesan = "[X200]["+str(response.status)+"] Data jari "+str(pegawai_.id_fingerprint)+" "+str(pegawai_)+" GAGAL diambil dari server dengan ID Fingerprint "+str(sett_.api_user_name)+"."
			except drest.exc.dRestAPIError :
				resp.pesan = '[X100][1]. Koneksi server error, coba beberapa saat kemudian.'
		except drest.exc.dRestRequestError :
			resp.pesan = '[X100][2]. Koneksi server error, coba beberapa saat kemudian.'
	except Exception :
		resp.pesan = '[X100][3]. Koneksi server error, coba beberapa saat kemudian.'
	if not resp.success:
		save_log('Error', 'sinkronisasi_jari_pegawai', resp.pesan, sett_)
	return resp
def delete_user_mesin(pegawai_, sett_):
	resp = holder()
	command = '<DeleteUser>\r\n'+'<ArgComKey xsi:type="xsd:integer">'+str(sett_.key_fingerprint)+'</ArgComKey>\r\n'
	command = command + '<Arg><PIN xsi:type="xsd:integer">'+str(pegawai_.id_fingerprint)+'</PIN></Arg>\r\n'
	command = command +'</DeleteUser>\r\n'+'\r\n'
	resp = send_command(command, sett_)
	return resp
def hapus_data_mesin_yang_tidak_ada_disiabjo(sett_):
	resp = holder()
	server_base_url = sett_.api_url.split("/api/")[0]+"/pegawai/fingerprint/"+str(sett_.api_user_name)
	pegawai_list = ''
	
	try:
		r = requests.get(server_base_url)
		pegawai_list = r.content
	except ConnectionError as e:		
		resp_send_log = send_log('Error', 'hapus_data_mesin_yang_tidak_ada_disiabjo', '[X100].Tidak dapat konek ke server.', sett_, None, True)
	if pegawai_list and pegawai_list != '':
		pegawai = Pegawai.objects.exclude(id_fingerprint__in=pegawai_list.split(',')).filter(setting=sett_)
		for p in pegawai:
			if p.sinkron_mesin:
				resp = delete_user_mesin(p, sett_)
				if resp.success:
					p.delete()
					resp_send_log = send_log('Warning', 'hapus_data_mesin_yang_tidak_ada_disiabjo', '[X201]. Data pegawai '+str(p)+' dihapus dari Mesin '+str(sett_.api_user_name)+'.', sett_, None, True)
	return resp

def sinkronisasi_pegawai():
	resp = holder()
	sett_list = get_setting()
	for sett in sett_list:
		try :
			try :
				try :
					api = drest.api.TastyPieAPI(sett.api_url)
					api.auth(sett.api_user_name, sett.api_key)
					get_params = dict(fingerprint = sett.api_user_name, limit=0,)
					data = api.pegawai.get(params=get_params)
					
					if data.status == 200 :
						try :
							obj_data = data.data['objects']
							# print obj_data
							for i in obj_data:
								pesan_ = ""
								p, created = Pegawai.objects.get_or_create(id_fingerprint=i['id'])
								p.nip = i['nomor_identitas']
								p.nama = i['nama_lengkap']
								p.pin = i['pin']
								p.privilege = i['privilege']
								p.gelar_depan = i['gelar_depan']
								p.gelar_belakang = i['gelar_belakang']
								if i['jabatan']:
									p.jabatan = str(i['jabatan']).replace('<br />', '')
								p.foto = i['foto']
								if created:
									p.setting.add(sett)
									# added to raspberry
									pesan_ = "[X300] Data "+str(p)+" telah disimpan ke raspberry."
								elif p.setting:
									if not p.setting.all().filter(id=sett.id).exists():
										p.setting.add(sett)
								p.save()
								print p
								respon_jari = sinkronisasi_jari_pegawai(p, sett)
								jumlah_jari_after = p.fingerprinttemplate_set.all().count()
								if respon_jari.success:
									if not p.fingerprinttemplate_set.all().filter(sinkron_server=False).exists():
										p.sinkron_server=True
										p.sinkron_mesin =True
										p.save()
								else:
									if jumlah_jari_after == 0:
										p.sinkron_server=True
										p.sinkron_mesin =True
										p.save()

								# kirim ke mesin
								if p.sinkron_mesin:
									resp = set_userinfo_mesin(sett, [p,], False)
									if resp.success:
										refresh_db(sett)
										resp = set_jari_mesin(p, sett)
										if resp.success:
											refresh_db(sett)
									if created and resp.success:
										pesan_ = pesan_ + " Data telah dikirim ke mesin "+str(sett.api_user_name)+"."

								# Simpan log penambahan
								if pesan_ != "":
									save_log('Warning', 'sinkronisasi_pegawai', pesan_, sett)

							resp.pesan = '[X300]. Sinkronisasi pegawai berhasil.'
							resp.success = True
						except KeyError :
							resp.pesan = '[X200]. Sinkronisasi gagal, data yg diterima tidak sesuai.'
					else:
						resp.pesan = '[X200]. Koneksi server error, coba beberapa saat kemudian.'
				except drest.exc.dRestAPIError :
					resp.pesan = '[X100][1]. Koneksi server error, coba beberapa saat kemudian.'
			except drest.exc.dRestRequestError :
				resp.pesan = '[X100][2]. Koneksi server error, coba beberapa saat kemudian.'
		except Exception :
			resp.pesan = '[X100][3]. Koneksi server error, coba beberapa saat kemudian.'
		if resp.success:
			hapus_data_mesin_yang_tidak_ada_disiabjo(sett)
			hapus_log()
		else:
			save_log('Error', 'sinkronisasi_pegawai', resp.pesan, sett)
	return resp

def ambil_waktu(sett_):
	resp = holder()
	try :	
		command = '<GetDate>\r\n'+'<ArgComKey xsi:type="xsd:integer">'+str(sett_.key_fingerprint)+'</ArgComKey>\r\n'+'</GetDate>\r\n'+'\r\n'
		resp = send_command(command, sett_)
	except Exception :
		resp_send_log = send_log('Error', 'ambil_waktu', '[X101]. Tidak dapat konek ke mesin fingerprint.', sett_, True)
	return resp

def send_absensi_v2(waktu_, tipe_waktu_, id_fingerprint, sett_):	
	resp = holder()
	try :
		try :
			try :
				api = drest.api.TastyPieAPI(sett_.api_url)
				api.auth(sett_.api_user_name, sett_.api_key)
				api.request.add_header('X-CSRFToken', '{{csrf_token}}')
				api.request.add_header('Content-Type','application/json')
				# data_absensi = dict(fingerprint=sett_.api_user_name, waktu = waktu_, tipe_waktu = tipe_waktu_, id_fingerprint=pegawai_.id_fingerprint, nomor_identitas='')
				data_absensi = dict(fingerprint=sett_.api_user_name, waktu = waktu_, tipe_waktu = tipe_waktu_, id_fingerprint=id_fingerprint, nomor_identitas='')
				response = api.absensi.post(data_absensi)
				if response.status in (200, 201, 202) :
					resp.pesan = "[X300]["+str(response.status)+"] Data absensi "+str(id_fingerprint)+" "+str(tipe_waktu_)+" "+str(waktu_)+" BERHASIL dikirim ke server dengan ID Fingerprint "+str(sett_.api_user_name)+"."
					resp.success = True
				else :
					resp.pesan = "[X200]["+str(response.status)+"] Data absensi "+str(id_fingerprint)+" "+str(tipe_waktu_)+" "+str(waktu_)+" GAGAL dikirim ke server dengan ID Fingerprint "+str(sett_.api_user_name)+"."
			except Exception :
				resp.pesan = '[X200] Koneksi server error 1.'
		except drest.exc.dRestAPIError :
			resp.pesan = '[X200] Koneksi server error 2.'
	except drest.exc.dRestRequestError :
		resp.pesan = '[X200] Koneksi server error 3.'
	if not resp.success:
		save_log('Error', 'send_absensi_v2', resp.pesan, sett_)
	return resp

def send_data_absensi(fp_):
	# resp_send_absensi = send_absensi(fp_.waktu.strftime('%Y-%m-%d %H:%M:%S'), fp_.tipe_waktu, fp_.pegawai.nip)
	# sett = get_setting()
	sett = fp_.get_setting()
	resp_send_absensi = send_absensi_v2(fp_.waktu.strftime('%Y-%m-%d %H:%M:%S'), fp_.tipe_waktu, fp_.get_id(), fp_.get_setting())	
	if not resp_send_absensi.success:
		save_log('Error', 'send_absensi_v2', resp_send_absensi.pesan, sett)
		# Jika data absensi dari pegawai yg tidak dikenali akan dihapus langsung
		if '[501]' in resp_send_absensi.pesan:
			fp_.delete()
	else:
		fp_.terkirim = True
		fp_.save()
	return resp_send_absensi

def hapus_data_di_mesin(sett_):
	resp = holder()
	command = '<ClearData>\r\n'+'<ArgComKey xsi:type="xsd:integer">'+str(sett_.key_fingerprint)+'</ArgComKey>\r\n'
	command = command + '<Arg><Value xsi:type="xsd:integer">3</Value></Arg>\r\n'
	command = command +'</ClearData>\r\n'+'\r\n'
	respon = send_command(command, sett_)
	if respon.success:
		resp.pesan = "Hapus data di mesin selesai"
		resp.success = True
		refresh_db(sett_)
	else:
		resp_send_log = send_log('Error', 'hapus_data_di_mesin', respon.pesan, sett_, None, True)
	return resp

def sync_waktu():
	resp = holder()
	sett_list = get_setting()
	for sett in sett_list:
		req = None
		req = get_server_time(sett)
		if req:
			waktu = req.content
			if waktu != '' and len(waktu.split(' ')) == 2:
				code_ = None
				code_ = commands.getstatusoutput('service ntp stop && ntpdate -s ntp.ubuntu.com && service ntp start')
				if code_ and code_[0] == 256:
					code_ = None
					code_ = commands.getstatusoutput('service ntp stop && ntpdate -s pool.ntp.org && service ntp start')
				if code_ and code_[0] == 256:
					code_ = None
					code_ = commands.getstatusoutput('service ntp stop && ntpdate -s time.nist.gov && service ntp start')
				if code_ and code_[0] == 0:
					check_waktu = ambil_waktu(sett)
					if check_waktu.success:
						tree = ElementTree.fromstring(check_waktu.pesan)
						waktu_mesin = tree.findall("Row")
						for w_ in waktu_mesin:
							tanggal_mesin = w_[0].text
							jam_mesin = w_[1].text
							waktu_sekarang = datetime.datetime.now()
							waktu_sekarang = pytz.timezone("GMT").localize(waktu_sekarang)
							# waktu_server = datetime.datetime.strptime(waktu, "%Y-%m-%d %H:%M:%S")
							# waktu_server = waktu_server+req.elapsed
							# waktu_server = pytz.timezone("GMT").localize(waktu_server)
							datetime_mesin = datetime.datetime.strptime(tanggal_mesin+' '+jam_mesin, "%Y-%m-%d %H:%M:%S")
							datetime_mesin = pytz.timezone("GMT").localize(datetime_mesin)
							# jika lebih dari 1 menit
							if abs((datetime_mesin-waktu_sekarang).total_seconds())>60:		
								command = '<SetDate>\r\n'+'<ArgComKey xsi:type="xsd:integer">'+str(sett.key_fingerprint)+'</ArgComKey>\r\n'
								command = command + '<Arg><Date xsi:type="xsd:string">'+waktu_sekarang.strftime("%Y-%m-%d")+'</Date><Time xsi:type="xsd:string">'+waktu_sekarang.strftime("%H:%M:%S")+'</Time></Arg>\r\n'
								command = command + '</SetDate>\r\n'+'\r\n'
								respon = send_command(command, sett)
								if respon.success:
									restart_mesin(sett)
									if resp.pesan == '':
										resp.pesan = "Sinkronisasi Waktu selesai. Restart Mesin "+str(sett.ip_fingerprint)
									else:
										resp.pesan += ", Sinkronisasi Waktu selesai. Restart Mesin "+str(sett.ip_fingerprint)
									resp.success = True
								else:
									resp_send_log = send_log('Error', 'sync_waktu', respon.pesan, sett, None, True)
									resp.pesan = "Tidak dapat menyambung ke mesin. Harap periksa jaringan kabel atau sambungan ke mesin."
							else:
								resp.pesan = "Waktu sudah sesuai. Tidak ada pembaruhan waktu di mesin. Sinkronisasi selesai."
					else:
						resp.pesan = "Tidak dapat menyambung ke mesin. Harap periksa jaringan kabel atau sambungan ke mesin."
				else:
					resp_send_log = send_log('Error', 'sync_waktu', '[X400] Tidak dapat konek ke internet.', sett, None, True)
					resp.pesan = "Tidak dapat konek ke internet. Check koneksi internet anda. Pastikan bisa nyambung ke google.com, kedirikab.go.id, dan siabjo.kedirikab.go.id"
			else:
				resp.pesan = "Terdapat masalah dengan koneksi siabjo. Harap check koneksi ke siabjo."
		else:
			resp.pesan = "Tidak dapat konek ke siabjo. Harap check koneksi ke siabjo."
	return resp

def hapus_attlog():
	sett_list = get_setting()
	for sett in sett_list:
		command = '<GetAttLog>\r\n'+'<ArgComKey xsi:type="xsd:integer">'+str(sett.key_fingerprint)+'</ArgComKey>\r\n'
		command = command + '<Arg><PIN xsi:type="xsd:integer">All</PIN></Arg>\r\n'
		command = command +'</GetAttLog>\r\n'+'\r\n'
		respon = send_command(command, sett)
		if respon.success:
			tree = ElementTree.fromstring(respon.pesan)
			fingerprints = tree.findall("Row")

			counter_error = 0
			jumlah_data = 0
			for fingerprint in fingerprints:
				datetime_obj = datetime.datetime.strptime(fingerprint[1].text, "%Y-%m-%d %H:%M:%S")
				datetime_obj = pytz.timezone("GMT").localize(datetime_obj)
				id_fingerprint = fingerprint[0].text
				tipe_waktu_ = fingerprint[3].text
				fp_olds = Fingerprint.objects.filter(waktu=datetime_obj, id_fingerprint=id_fingerprint, tipe_waktu=tipe_waktu_)
				jumlah_data = jumlah_data + 1
				if not fp_olds.exists():
					try :
						tempFingerprint = Fingerprint(waktu=datetime_obj, tipe_waktu=tipe_waktu_, id_fingerprint=id_fingerprint, setting=sett)
						tempFingerprint.save()
						if tempFingerprint.pk is None :
							counter_error=counter_error+1
							save_log('Error', 'saving fingerprint 1', '[X201]['+str(tempFingerprint)+'] Penyimpanan data fingerprint error.', sett)
					except Exception :
						counter_error=counter_error+1
						save_log('Error', 'saving fingerprint 2', '[X201]['+str(id_fingerprint)+' - '+str(datetime_obj)+'] Penyimpanan data fingerprint error.', sett)
			
			if jumlah_data > 0 and counter_error == 0:
				resp = hapus_data_di_mesin(sett)
				if resp.success:
					pesan_ = "["+str(jumlah_data)+" Data] "+str(resp.pesan)
					resp_send_log = send_log('Warning', 'hapus_data_di_mesin', pesan_, sett, None, True)
		else:
			resp_send_log = send_log('Error', 'hapus_attlog', respon.pesan, sett, None, True)
	return True

def crawler_data():
	sett_list = get_setting()
	for sett in sett_list:
		# print sett
		print "1111"+str(sett.key_fingerprint)+"2222"
		command = '<GetAttLog>\r\n'+'<ArgComKey xsi:type="xsd:integer">'+str(sett.key_fingerprint)+'</ArgComKey>\r\n'
		command = command + '<Arg><PIN xsi:type="xsd:integer">All</PIN></Arg>\r\n'
		command = command +'</GetAttLog>\r\n'+'\r\n'
		respon = send_command(command, sett)

		if respon.success:
			tree = ElementTree.fromstring(respon.pesan)
			fingerprints = tree.findall("Row")

			counter_error = 0
			for fingerprint in fingerprints:
				datetime_obj = datetime.datetime.strptime(fingerprint[1].text, "%Y-%m-%d %H:%M:%S")
				datetime_obj = pytz.timezone("GMT").localize(datetime_obj)
				id_fingerprint = fingerprint[0].text
				tipe_waktu_ = fingerprint[3].text
				fp_olds = Fingerprint.objects.filter(waktu=datetime_obj, id_fingerprint=id_fingerprint, tipe_waktu=tipe_waktu_)
				if not fp_olds.exists():
					try :
						tempFingerprint = Fingerprint(waktu=datetime_obj, tipe_waktu=tipe_waktu_, id_fingerprint=id_fingerprint, setting=sett)
						tempFingerprint.save()
						if tempFingerprint.pk is None :
							counter_error=counter_error+1
							save_log('Error', 'saving fingerprint 1', '[X201]['+str(tempFingerprint)+'] Penyimpanan data fingerprint error.', sett)
					except Exception :
						counter_error=counter_error+1
						save_log('Error', 'saving fingerprint 2', '[X201]['+str(id_fingerprint)+' - '+str(datetime_obj)+'] Penyimpanan data fingerprint error.', sett)
		else:
			resp_send_log = send_log('Error', 'crawler_data', respon.pesan, sett, None, True)
	return True

def get_datalokal():
	# now = datetime.datetime.now()
	# now += timedelta(hours=7)
	fingerprints = list(Fingerprint.objects.filter(terkirim=False).order_by('waktu'))
	if fingerprints:
		for fingerprint in fingerprints :
			resp_send_absensi = send_data_absensi(fingerprint)
			if resp_send_absensi.success:
				fingerprint.terkirim = True
				fingerprint.save()
		resp = "Kirim data absensi yang belum terkirim."

	resp_sinyal = holder()
	sett_list = get_setting()
	for sett in sett_list:
		try :
			try :
				try :
					api = drest.api.TastyPieAPI(sett.api_url)
					api.auth(sett.api_user_name, sett.api_key)
					api.request.add_header('X-CSRFToken', '{{csrf_token}}')
					api.request.add_header('Content-Type','application/json')
					interfaces = ni.interfaces()
					ip_rasp = ''
					# if '{6A1E62EF-7996-4B7C-86F8-C68421F86D08}' in interfaces:
					# 	ip_rasp = ni.ifaddresses('{6A1E62EF-7996-4B7C-86F8-C68421F86D08}')[2][0]['addr']
					# if 'eth0' in interfaces:
					# 	ip_rasp = ni.ifaddresses('eth0')[2][0]['addr']
					# if 'em5' in interfaces:
					# 	ip_rasp = ni.ifaddresses('em5')[2][0]['addr']
					if len(interfaces) > 0:
						ip_rasp = ";".join(x+':'+ni.ifaddresses(x)[2][0]['addr'] for x in interfaces)
					get_params = dict(fingerprint = sett.api_user_name, ipmesin = sett.ip_fingerprint, ipraspberry = ip_rasp)
					response = api.fp.get(params=get_params) #Update sinyal
					if response.status in (200, 201, 202) :
						resp_sinyal.pesan = "[X200]["+str(response.status)+"] Mengirim sinyal ke server berhasil."
						resp_sinyal.success = True
						try :
							obj_data = response.data['objects']
							for i in obj_data:
								cur_ver = get_version()
								versi_raspberry = i['versi_raspberry']
								if cur_ver and versi_raspberry:
									if cur_ver != '' and versi_raspberry != '':
										if mycmp(cur_ver, versi_raspberry) < 0:	
											# Nanti perlu ada pengecheckan berhasil atau tidak
											try :
												batcmd="chmod a+x reset_server.sh && ./reset_server.sh"
												# result = subprocess.check_output(batcmd, shell=True)
												os.system(batcmd)
												set_version(versi_raspberry)
											except Exception :
												resp_send_log = send_log('Error', 'get_datalokal', 'Update raspberry failed.', sett, None, True)
						except KeyError :
							pass
					else :
						resp_sinyal.pesan = "[X200]["+str(response.status)+"] Mengirim sinyal ke server gagal."
				except drest.exc.dRestAPIError :
					resp_sinyal.pesan = '[X200] Koneksi server error 1.'
			except drest.exc.dRestRequestError :
				resp_sinyal.pesan = '[X200] Koneksi server error 2.'
		except Exception :
			resp_sinyal.pesan = '[X200] Koneksi server error 3.'
		resp = resp_sinyal.pesan
		if not resp_sinyal.success:
			save_log('Error', 'kirim_sinyal', resp_sinyal.pesan, sett)
	return resp

def check_koneksi(url_):
	req = None
	try:
		req = requests.get(url_)
	except Exception as e:
		req = None
	return req

def display(request):
	data_absensi = Fingerprint.objects.all().order_by('-waktu')
	sett_list = get_setting()
	pesan = ""
	code_ = ""
	act = request.GET.get('act', None)
	
	if act:
		if act == "sinkron_pegawai":
			resp = sinkronisasi_pegawai()
			pesan = resp.pesan
		elif act == "download_absensi":
			file_pre = "files/static-collected/presensi"
			f = open(file_pre, 'w+')
			data_a = data_absensi.filter(terkirim=False)
			for da in data_a:
				f.write(str(da.id_fingerprint)+","+str(da.waktu.strftime('%Y-%m-%d %H:%M:%S'))+","+str(da.tipe_waktu)+","+str(da.setting.api_user_name)+"\r")
			f.close()
			sett_list = get_setting()
			sett = sett_list.last()
			file_name = file_pre+"-"+sett.api_user_name+".enc"
			encrypt_file(sett.get_api_key(), file_pre, file_name)
			os.remove(file_pre)
			url_re = file_name.replace('files/static-collected/','/static/')
			return HttpResponseRedirect(url_re)

		elif act == "ambil_absensi":
			# code_ = commands.getstatusoutput('chmod a+x reset_server.sh && ./reset_server.sh')
			resp = crawler_data()
			pesan = "Ambil data dari Mesin selesai.."

		elif act == "sinkron_waktu_mesin":
			resp = sync_waktu()
			pesan = resp.pesan

		elif act == "check_koneksi":
			sett_list = get_setting()
			for sett in sett_list:
				r = None
				r = check_koneksi('http://'+str(sett.ip_fingerprint)+"/")
				if r and r.status_code in (200, 201, 202):
					pesan += "Koneksi Fingerprint "+str(sett.ip_fingerprint)+" <font color='green'><b><u>BERHASIL</u></b></font>"
				else:
					pesan += "Koneksi Fingerprint "+str(sett.ip_fingerprint)+" <font color='#f39c12'><b><u>GAGAL</u></b></font>. Klik <a href='static/Memastikan Koneksi Mesin.pdf'>Disini</a> Untuk Lihat Panduan."

				r = None
				r = get_server_time(sett)
				server_base_url = sett.api_url.split("/api/")[0]
				if r:
					waktu = r.content
					if waktu != '' and len(waktu.split(' ')) == 2:
						pesan += "<br />Koneksi server siabjo.kedirikab.go.id <font color='green'><u><b>BERHASIL</b></u></font><br />"
					else:
						pesan += "<br />Koneksi server siabjo.kedirikab.go.id <font color='#f39c12'><u><b>GAGAL</b></u></font>. Pastikan Anda bisa mengakses http://siabjo.kedirikab.go.id atau "+server_base_url+".<br />"
				else:
					pesan += "<br />Koneksi server siabjo.kedirikab.go.id <font color='#f39c12'><u><b>GAGAL</b></u></font>. Pastikan Anda bisa mengakses http://siabjo.kedirikab.go.id atau "+server_base_url+".<br />"

			pesan = mark_safe(pesan)
		elif act == "sinkron_waktu_raspberry":
			code_ = commands.getstatusoutput('service ntp stop && ntpdate -s pool.ntp.org && service ntp start')

		elif act == "restart_mesin":
			sett_list = get_setting()
			for sett in sett_list:
				resp = restart_mesin(sett)
				pesan = "Restart Mesin..."

		elif act == "update_rasberry":
			code_ = commands.getstatusoutput('chmod a+x reset_server.sh && ./reset_server.sh')

		elif act == "restart_web_server":
			code_ = commands.getstatusoutput('chmod a+x restart_server.sh && ./restart_server.sh')

		elif act == "run_openvpn":
			code_ = commands.getstatusoutput('service openvpn start')

		elif act == "stop_openvpn":
			code_ = commands.getstatusoutput('service openvpn stop')

		elif act == "restart_raspberry":
			code_ = commands.getstatusoutput('reboot')

		elif act == "shutdown_raspberry":
			code_ = commands.getstatusoutput('init 0')

		elif act == "kirim_absensi":
			fingerprints = list(Fingerprint.objects.filter(terkirim=False).order_by('waktu'))
			
			if fingerprints:
				for fingerprint in fingerprints :
					resp_send_absensi = send_data_absensi(fingerprint)
					if resp_send_absensi.success:
						fingerprint.terkirim = True
						fingerprint.save()
					if pesan == "":
						pesan = "[Data Absensi "+str(fingerprint)+"] "+resp_send_absensi.pesan
					else:
						pesan += ", [Data Absensi "+str(fingerprint)+"] "+resp_send_absensi.pesan

	now = datetime.datetime.now()
	# sett = get_setting()
	data_absensi = Fingerprint.objects.all().order_by('-waktu')
	per_page = 100
	paginator = Paginator(data_absensi, per_page) 
	page = request.GET.get('page')
	try:
		data_absensi = paginator.page(page)
		page = int(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		data_absensi = paginator.page(1)
		page = 1
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		data_absensi = paginator.page(paginator.num_pages)
	# waktu = get_server_time(sett.last())

	request.GET = request.GET.copy()
	if 'act' in request.GET:
		del request.GET['act']
	template = loader.get_template('display.html')
	context = RequestContext(request, {
		'tanggal' : str(babel.dates.format_date(now, 'EEEE, d MMMM Y', locale='in_ID')),
		'data_absensi' : data_absensi,
		'pesan' : pesan,
		'kode' : code_,
		'sett_list' : sett_list,
		# 'waktu' : waktu,
		})
	return HttpResponse(template.render(context))

# menghapus data di table Fingerprint yang sudah terkirim 2 bulan lalu
def hapus_fingerprint():
	#Fingerprint.objects.exclude(waktu__startswith=now.date()).delete()
	one_month_ago = datetime.datetime.now() - relativedelta(months=1)
	one_month_ago = one_month_ago - datetime.timedelta(days=one_month_ago.day-1)
	Fingerprint.objects.filter(waktu__lt=one_month_ago.strftime('%Y-%m-%d'), terkirim=True).delete()
	return "Hapus data fingerprint"
	
