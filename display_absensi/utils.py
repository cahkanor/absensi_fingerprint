from display_absensi.models import Setting
from django.db.utils import OperationalError
import re, os

def num(s):
	if s:
		try:
			return int(s)
		except ValueError:
			return 0
	return 0

def mycmp(version1, version2):
	def normalize(v):
		return [int(x) for x in re.sub(r'(\.0+)*$','', v).split(".")]
	return cmp(normalize(version1), normalize(version2))

def set_version(text):
	file_ve = '../vesion.txt'
	if text:
		try:
			f = open(file_ve, 'w+')
			f.write(text)
			f.close()
		except Exception :
			os.system('chmod 777 '+file_ve)
			f = open(file_ve, 'w+')
			f.write(text)
			f.close()

def get_version():
	cur_version = ''
	file_ve = '../vesion.txt'
	try:
		if os.path.isfile(file_ve):
			f = open(file_ve, 'r')
			for line in f:
				cur_version = cur_version+str(line)
			f.close()
		else:
			set_version('0')
	except Exception :
		os.system('chmod 777 '+file_ve)
		f = open(file_ve, 'r')
		for line in f:
			cur_version = cur_version+str(line)
		f.close()
	return cur_version

def get_setting():
	sett = Setting.objects.filter(aktif=True)
	if not sett.exists():
		sett = Setting.objects.all()[:1]
	return sett

import random, struct, base64
from Crypto.Cipher import AES

def encrypt_file(key, in_filename, out_filename=None, chunksize=64*1024):
	""" Encrypts a file using AES (CBC mode) with the
		given key.

		key:
			The encryption key - a string that must be
			either 16, 24 or 32 bytes long. Longer keys
			are more secure.

		in_filename:
			Name of the input file

		out_filename:
			If None, '<in_filename>.enc' will be used.

		chunksize:
			Sets the size of the chunk which the function
			uses to read and encrypt the file. Larger chunk
			sizes can be faster for some files and machines.
			chunksize must be divisible by 16.
	"""
	if not out_filename:
		out_filename = in_filename + '.enc'

	iv = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))
	encryptor = AES.new(key, AES.MODE_CBC, iv)
	filesize = os.path.getsize(in_filename)

	with open(in_filename, 'rb') as infile:
		with open(out_filename, 'wb') as outfile:
			outfile.write(struct.pack('<Q', filesize))
			outfile.write(iv)

			while True:
				chunk = infile.read(chunksize)
				if len(chunk) == 0:
					break
				elif len(chunk) % 16 != 0:
					chunk += ' ' * (16 - len(chunk) % 16)

				outfile.write(encryptor.encrypt(chunk))

def AESencrypt(password, plaintext, base64=False):
	import hashlib, os
	from Crypto.Cipher import AES
	SALT_LENGTH = 32
	DERIVATION_ROUNDS=1337
	BLOCK_SIZE = 16
	KEY_SIZE = 32
	MODE = AES.MODE_CBC
	 
	salt = os.urandom(SALT_LENGTH)
	iv = os.urandom(BLOCK_SIZE)
	
	paddingLength = 16 - (len(plaintext) % 16)
	paddedPlaintext = plaintext+chr(paddingLength)*paddingLength
	derivedKey = password
	for i in range(0,DERIVATION_ROUNDS):
		derivedKey = hashlib.sha256(derivedKey+salt).digest()
	derivedKey = derivedKey[:KEY_SIZE]
	cipherSpec = AES.new(derivedKey, MODE, iv)
	ciphertext = cipherSpec.encrypt(paddedPlaintext)
	ciphertext = ciphertext + iv + salt
	if base64:
		import base64
		return base64.b64encode(ciphertext)
	else:
		return ciphertext.encode("hex")
 
def AESdecrypt(password, ciphertext, base64=False):
	import hashlib
	from Crypto.Cipher import AES
	SALT_LENGTH = 32
	DERIVATION_ROUNDS=1337
	BLOCK_SIZE = 16
	KEY_SIZE = 32
	MODE = AES.MODE_CBC
	 
	if base64:
		import base64
		decodedCiphertext = base64.b64decode(ciphertext)
	else:
		decodedCiphertext = ciphertext.decode("hex")
	startIv = len(decodedCiphertext)-BLOCK_SIZE-SALT_LENGTH
	startSalt = len(decodedCiphertext)-SALT_LENGTH
	data, iv, salt = decodedCiphertext[:startIv], decodedCiphertext[startIv:startSalt], decodedCiphertext[startSalt:]
	derivedKey = password
	for i in range(0, DERIVATION_ROUNDS):
		derivedKey = hashlib.sha256(derivedKey+salt).digest()
	derivedKey = derivedKey[:KEY_SIZE]
	cipherSpec = AES.new(derivedKey, MODE, iv)
	plaintextWithPadding = cipherSpec.decrypt(data)
	paddingLength = ord(plaintextWithPadding[-1])
	plaintext = plaintextWithPadding[:-paddingLength]
	return plaintext