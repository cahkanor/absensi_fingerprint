from django.db import models
from django.contrib.auth.models import User
from mptt.models import MPTTModel
from mptt.fields import TreeForeignKey
import display_absensi
import pytz
from datetime import timedelta

class Setting(models.Model):
	ip_fingerprint = models.GenericIPAddressField("IP Fingerprint", default="0.0.0.0")
	key_fingerprint = models.CharField("Key Fingerprint", max_length=20, default="0")
	api_url = models.URLField("API URL", max_length=200, default="http://siabjo.kedirikab.go.id/api/v2/");
	api_user_name = models.CharField("API Username", max_length=50, default="cominfo");
	api_key = models.CharField("API Key", max_length=255, default="2BPt3Cgq30");
	aktif = models.BooleanField(default=False, help_text='Settingan ini dipakai atau tidak? Jika dipakai yang lain akan dinonaktifkan')
	
	# otomatis = models.BooleanField(default=True, help_text='Sinkron Otomatis, ketika settingan ini diaktifkan maka semua data untuk fingerprint dengan API Username yang dimasukkan akan melakukan sinkronisasi otomatis dan tidak diijinkan untuk melakukan sinkronisasi manual.')

	def get_api_key(self):
		key_ = self.api_key.replace('-','')
		return key_[len(key_)/2:]

	def __unicode__(self):
		return str(self.ip_fingerprint)+" - "+str(self.api_url)

	class Meta:
		verbose_name='Setting'
		verbose_name_plural='Setting'

class Pegawai(models.Model):
	class Meta:
		verbose_name='Pegawai'
		verbose_name_plural='Pegawai'

	nip = models.CharField("NIP", max_length=30, unique=True, null=True, blank=True)
	pin = models.CharField(verbose_name="PIN", max_length=255, null=True, blank=True)
	privilege = models.IntegerField(verbose_name="Privilege", default=0)
	nama = models.CharField("Nama", max_length=100, null=True, blank=True)
	gelar_depan = models.CharField("Gelar Depan", max_length=30, null=True, blank=True)
	gelar_belakang = models.CharField("Gelar Belakang", max_length=30, null=True, blank=True)
	foto = models.ImageField(upload_to='static/profiles/', max_length=255, null=True, blank=True)
	jabatan = models.CharField("Jabatan", max_length=255, null=True, blank=True)
	id_fingerprint = models.IntegerField("ID Fingerprint", default=0, unique=True)
	setting = models.ManyToManyField(Setting, blank=True)
	sinkron_server = models.BooleanField(default=False)
	sinkron_mesin = models.BooleanField(default=False)

	def get_foto(self):
		if self.foto:
			sett = display_absensi.utils.get_setting()
			if sett.exists():
				sett = sett[0]
				server_base_url = sett.api_url.split("/api/")
				return str(server_base_url[0])+str(self.foto)
		return "/static/web/images/person.svg"

	def __unicode__(self):
		if self.nama:
			return self.nama
		else:
			return str(self.id)

class FingerprintTemplate(models.Model):
	pegawai = models.ForeignKey(Pegawai, to_field='id_fingerprint', null=True, blank=True) #PIN atau ID Pegawai
	fingerprint_id = models.IntegerField(verbose_name='FingerID', default=0) #ID Jari Pegawai
	size = models.IntegerField(verbose_name='Size', default=0)
	valid = models.IntegerField(verbose_name='Valid', default=0)
	template = models.TextField(verbose_name='Template')
	sinkron_server = models.BooleanField(default=False)
	sinkron_mesin = models.BooleanField(default=False)

	class Meta:
		verbose_name='Fingerprint Template'
		verbose_name_plural='Fignerprint Template'

class Fingerprint(models.Model):
	id_fingerprint = models.IntegerField(null=True, blank=True)
	waktu = models.DateTimeField("Waktu")
	tipe_waktu = models.IntegerField()
	pegawai = models.ForeignKey(Pegawai, related_name="fingerprint_pegawai", to_field='id_fingerprint', null=True, blank=True)
	terkirim = models.BooleanField(default=False)
	
	setting = models.ForeignKey(Setting, null=True, blank=True)
	def get_setting(self):
		if self.setting:
			return self.setting
		else:
			sett = display_absensi.utils.get_setting()
			if sett.exists():
				sett = sett[0]
				return sett
		return None
	
	def get_pegawai(self):
		if self.pegawai:
			return self.pegawai
		else:
			p = Pegawai.objects.filter(id_fingerprint=self.id_fingerprint)
			if p.exists():
				peg = p.last()
				self.pegawai = peg
				self.save()
				return peg
			else:
				return None

	def get_nama_pegawai(self):
		peg = self.get_pegawai()
		if peg:
			return peg.nama
		else:
			return "-"

	def get_nip(self):
		peg = self.get_pegawai()
		if peg:
			return peg.nip
		else:
			return "-"

	def get_jabatan(self):
		peg = self.get_pegawai()
		if peg:
			return peg.jabatan
		else:
			return "-"

	def get_waktu(self):
		if self.waktu:
			return self.waktu.replace(tzinfo=pytz.timezone("Asia/Jakarta"))+timedelta(minutes=7)
		else:
			return "-"

	def get_id(self):
		if self.id_fingerprint:
			return self.id_fingerprint
		elif self.pegawai:
			return self.pegawai.id_fingerprint
		else:
			return 0

	def __unicode__(self):
		if self.pegawai:
			return str(self.pegawai.nip)+' - '+str(self.waktu)
		else:
			return str(self.waktu)

	class Meta:
		verbose_name='Data Absensi'
		verbose_name_plural='Data Absensi'

class FingerprintLog(models.Model):
	status = models.CharField("Status", max_length=100, blank=True, null=True) #Error Success Warning Danger...
	kategori = models.CharField("Kategori Log", max_length=100) #KategoriLog Jenis Log, Nama Fungsi, etc..
	log = models.TextField("Keterangan Log")
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	terkirim = models.BooleanField(default=False)
	setting = models.ForeignKey(Setting, null=True, blank=True)
	def get_setting(self):
		if self.setting:
			return self.setting
		else:
			sett = display_absensi.utils.get_setting()
			if sett.exists():
				sett = sett[0]
				return sett
		return None

	def __unicode__(self):
		return u'[%s] %s' % (self.kategori, self.log)

	class Meta:
		verbose_name='Log Fingerprint'
		verbose_name_plural='Log Fingerprint'

# Code X100 Error Koneksi Server
# Code X101 Error Koneksi Mesin
# Code X200 Error Data Server
# Code X201 Error Data Client
# Code X300 Data Sukses