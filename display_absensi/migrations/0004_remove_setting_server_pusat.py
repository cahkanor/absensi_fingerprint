# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0003_auto_20150104_1053'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='setting',
            name='server_pusat',
        ),
    ]
