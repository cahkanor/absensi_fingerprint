# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Fingerprint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('waktu', models.DateTimeField(verbose_name=b'Waktu')),
                ('tipe_waktu', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pegawai',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nip', models.CharField(unique=True, max_length=30, verbose_name=b'NIP')),
                ('nama', models.CharField(max_length=100, verbose_name=b'Nama')),
                ('gelar_depan', models.CharField(max_length=30, null=True, verbose_name=b'Gelar Depan', blank=True)),
                ('gelar_belakang', models.CharField(max_length=30, null=True, verbose_name=b'Gelar Belakang', blank=True)),
                ('id_fingerprint', models.IntegerField(default=0, unique=True)),
            ],
            options={
                'verbose_name': 'Pegawai',
                'verbose_name_plural': 'Pegawai',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Setting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('id_terakhir', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='fingerprint',
            name='pegawai',
            field=models.ForeignKey(to='display_absensi.Pegawai', to_field=b'id_fingerprint'),
            preserve_default=True,
        ),
    ]
