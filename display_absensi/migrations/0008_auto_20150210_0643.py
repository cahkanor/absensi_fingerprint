# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0007_auto_20150210_0635'),
    ]

    operations = [
        migrations.AlterField(
            model_name='server',
            name='setting',
            field=models.OneToOneField(null=True, blank=True, to='display_absensi.Setting', verbose_name=b'Setting'),
            preserve_default=True,
        ),
    ]
