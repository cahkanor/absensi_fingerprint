# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0002_fingerprint'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='setting',
            name='id_terakhir',
        ),
        migrations.AddField(
            model_name='setting',
            name='ip_fingerprint',
            field=models.IPAddressField(default=b'0.0.0.0', verbose_name=b'IP Fingerprint'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='setting',
            name='key_fingerprint',
            field=models.CharField(default=b'0', max_length=20, verbose_name=b'Key Fingerprint'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='setting',
            name='kode_skpd',
            field=models.IntegerField(default=1, verbose_name=b'Kode SKPD'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='setting',
            name='server_pusat',
            field=models.CharField(default=b'http://absensi.adisatya-itconsultant.com', max_length=100, verbose_name=b'Alamat Server Pusat'),
            preserve_default=True,
        ),
        migrations.RunSQL("delete from display_absensi_setting"),
        migrations.RunSQL("insert into display_absensi_setting(id, ip_fingerprint, key_fingerprint, kode_skpd, server_pusat) values(1, '192.168.88.88', '0', 1, 'http://absensi.adisatya-itconsultant.com/api/v1/')"),
    ]
