# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0034_fingerprintlog_terkirim'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pegawai',
            name='setting',
            field=models.ManyToManyField(to='display_absensi.Setting', blank=True),
        ),
        migrations.AlterField(
            model_name='setting',
            name='ip_fingerprint',
            field=models.GenericIPAddressField(default=b'0.0.0.0', verbose_name=b'IP Fingerprint'),
        ),
    ]
