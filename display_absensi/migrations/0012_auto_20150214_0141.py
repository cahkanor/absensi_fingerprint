# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0011_setting_aktif'),
    ]

    operations = [
        migrations.AddField(
            model_name='pegawai',
            name='foto',
            field=models.ImageField(max_length=255, null=True, upload_to=b'static/profiles/', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='aktif',
            field=models.BooleanField(default=False, help_text=b'Settingan ini dipakai atau tidak? Jika dipakai yang lain akan dinonaktifkan'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='setting',
            name='api_key',
            field=models.CharField(default=b'2BPt3Cgq30', max_length=255, verbose_name=b'API Key'),
            preserve_default=True,
        ),
    ]
