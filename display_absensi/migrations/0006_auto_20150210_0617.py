# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0005_remove_setting_kode_skpd'),
    ]

    operations = [
        migrations.CreateModel(
            name='Server',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('api_url', models.URLField(default=b'http://siabjo.kedirikab.go.id/api/v2/', verbose_name=b'API URL')),
                ('api_user_name', models.CharField(default=b'cominfo', max_length=50, verbose_name=b'API Username')),
                ('api_key', models.CharField(default=b'2BPt3Cgq30', max_length=50, verbose_name=b'API Key')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='setting',
            options={'verbose_name': 'Setting', 'verbose_name_plural': 'Setting'},
        ),
        migrations.AddField(
            model_name='setting',
            name='server',
            field=models.ForeignKey(verbose_name=b'API Server', blank=True, to='display_absensi.Server', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='pegawai',
            name='id_fingerprint',
            field=models.IntegerField(default=0, unique=True, verbose_name=b'ID Fingerprint'),
            preserve_default=True,
        ),
    ]
