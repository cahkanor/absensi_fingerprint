# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0028_auto_20150514_1614'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pegawai',
            name='nama',
            field=models.CharField(max_length=100, null=True, verbose_name=b'Nama', blank=True),
            preserve_default=True,
        ),
    ]
