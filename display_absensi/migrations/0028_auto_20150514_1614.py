# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0027_auto_20150514_1604'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fingerprinttemplate',
            name='pin',
        ),
        migrations.RemoveField(
            model_name='pegawai',
            name='fingerprint',
        ),
        migrations.AddField(
            model_name='fingerprinttemplate',
            name='pegawai',
            field=models.ForeignKey(to_field=b'id_fingerprint', blank=True, to='display_absensi.Pegawai', null=True),
            preserve_default=True,
        ),
    ]
