# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0008_auto_20150210_0643'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='server',
            options={'verbose_name': 'Server', 'verbose_name_plural': 'Server'},
        ),
    ]
