# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0032_auto_20150516_1533'),
    ]

    operations = [
        migrations.RenameField(
            model_name='fingerprinttemplate',
            old_name='tersinkron',
            new_name='sinkron_mesin',
        ),
        migrations.RenameField(
            model_name='pegawai',
            old_name='tersinkron',
            new_name='sinkron_mesin',
        ),
        migrations.AddField(
            model_name='fingerprinttemplate',
            name='sinkron_server',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='pegawai',
            name='sinkron_server',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
