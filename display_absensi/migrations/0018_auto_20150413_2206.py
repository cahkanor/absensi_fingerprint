# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0017_setting_otomatis'),
    ]

    operations = [
        migrations.AlterField(
            model_name='setting',
            name='otomatis',
            field=models.BooleanField(default=True, help_text=b'Sinkron Otomatis, ketika settingan ini diaktifkan maka semua data untuk fingerprint yang dimasukkan akan melakukan sinkronisasi otomatis dan tidak diijinkan untuk melakukan sinkronisasi manual.'),
            preserve_default=True,
        ),
    ]
