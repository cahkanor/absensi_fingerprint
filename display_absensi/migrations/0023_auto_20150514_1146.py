# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0022_pegawai_setting'),
    ]

    operations = [
        migrations.CreateModel(
            name='FingerprintTemplate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('template', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='pegawai',
            name='fingerprint',
            field=models.ManyToManyField(related_name='fingerprint_template', null=True, to='display_absensi.FingerprintTemplate', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='fingerprint',
            name='pegawai',
            field=models.ForeignKey(related_name='fingerprint_pegawai', to_field=b'id_fingerprint', blank=True, to='display_absensi.Pegawai', null=True),
            preserve_default=True,
        ),
    ]
