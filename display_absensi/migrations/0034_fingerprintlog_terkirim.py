# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0033_auto_20150518_1444'),
    ]

    operations = [
        migrations.AddField(
            model_name='fingerprintlog',
            name='terkirim',
            field=models.BooleanField(default=False),
        ),
    ]
