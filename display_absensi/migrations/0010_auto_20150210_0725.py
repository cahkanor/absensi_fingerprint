# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0009_auto_20150210_0709'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='server',
            name='setting',
        ),
        migrations.DeleteModel(
            name='Server',
        ),
        migrations.AddField(
            model_name='setting',
            name='api_key',
            field=models.CharField(default=b'2BPt3Cgq30', max_length=50, verbose_name=b'API Key'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='setting',
            name='api_url',
            field=models.URLField(default=b'http://siabjo.kedirikab.go.id/api/v2/', verbose_name=b'API URL'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='setting',
            name='api_user_name',
            field=models.CharField(default=b'cominfo', max_length=50, verbose_name=b'API Username'),
            preserve_default=True,
        ),
    ]
