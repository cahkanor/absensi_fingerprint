# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0023_auto_20150514_1146'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='fingerprinttemplate',
            options={'verbose_name': 'Fingerprint Template', 'verbose_name_plural': 'Fignerprint Template'},
        ),
        migrations.AddField(
            model_name='fingerprinttemplate',
            name='fingerprint_id',
            field=models.IntegerField(default=0, verbose_name=b'FingerID'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='fingerprinttemplate',
            name='pin',
            field=models.IntegerField(default=0, verbose_name=b'PIN'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='fingerprinttemplate',
            name='size',
            field=models.IntegerField(default=0, verbose_name=b'Size'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='fingerprinttemplate',
            name='valid',
            field=models.IntegerField(default=0, verbose_name=b'Valid'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='fingerprinttemplate',
            name='template',
            field=models.TextField(verbose_name=b'Template'),
            preserve_default=True,
        ),
    ]
