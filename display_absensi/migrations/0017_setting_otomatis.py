# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0016_auto_20150308_1039'),
    ]

    operations = [
        migrations.AddField(
            model_name='setting',
            name='otomatis',
            field=models.BooleanField(default=True, help_text=b'Sinkron Otomatis, ketika settingan ini diaktifkan maka semua data untuk fingerprint yang dimasukkan akan sinkron otomatis.'),
            preserve_default=True,
        ),
    ]
