# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0021_fingerprint_id_fingerprint'),
    ]

    operations = [
        migrations.AddField(
            model_name='pegawai',
            name='setting',
            field=models.ManyToManyField(to='display_absensi.Setting', null=True, blank=True),
            preserve_default=True,
        ),
    ]
