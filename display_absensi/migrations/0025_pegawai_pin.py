# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0024_auto_20150514_1225'),
    ]

    operations = [
        migrations.AddField(
            model_name='pegawai',
            name='pin',
            field=models.IntegerField(default=0, verbose_name=b'PIN'),
            preserve_default=True,
        ),
    ]
