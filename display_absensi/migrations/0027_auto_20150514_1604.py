# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0026_pegawai_privilege'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pegawai',
            name='nip',
            field=models.CharField(max_length=30, unique=True, null=True, verbose_name=b'NIP', blank=True),
            preserve_default=True,
        ),
    ]
