# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0018_auto_20150413_2206'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='setting',
            name='otomatis',
        ),
    ]
