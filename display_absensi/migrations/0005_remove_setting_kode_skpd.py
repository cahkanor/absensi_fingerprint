# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0004_remove_setting_server_pusat'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='setting',
            name='kode_skpd',
        ),
    ]
