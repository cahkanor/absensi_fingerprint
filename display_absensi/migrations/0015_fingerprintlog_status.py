# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0014_auto_20150222_0801'),
    ]

    operations = [
        migrations.AddField(
            model_name='fingerprintlog',
            name='status',
            field=models.CharField(max_length=100, null=True, verbose_name=b'Status', blank=True),
            preserve_default=True,
        ),
    ]
