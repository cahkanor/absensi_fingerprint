# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0006_auto_20150210_0617'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='setting',
            name='server',
        ),
        migrations.AddField(
            model_name='server',
            name='setting',
            field=models.ForeignKey(verbose_name=b'Setting', blank=True, to='display_absensi.Setting', null=True),
            preserve_default=True,
        ),
    ]
