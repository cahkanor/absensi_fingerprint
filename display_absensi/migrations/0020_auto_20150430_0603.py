# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0019_remove_setting_otomatis'),
    ]

    operations = [
        migrations.AddField(
            model_name='fingerprint',
            name='setting',
            field=models.ForeignKey(blank=True, to='display_absensi.Setting', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='fingerprintlog',
            name='setting',
            field=models.ForeignKey(blank=True, to='display_absensi.Setting', null=True),
            preserve_default=True,
        ),
    ]
