# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0020_auto_20150430_0603'),
    ]

    operations = [
        migrations.AddField(
            model_name='fingerprint',
            name='id_fingerprint',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
