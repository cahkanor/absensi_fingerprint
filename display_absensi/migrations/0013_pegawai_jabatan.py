# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0012_auto_20150214_0141'),
    ]

    operations = [
        migrations.AddField(
            model_name='pegawai',
            name='jabatan',
            field=models.CharField(max_length=255, null=True, verbose_name=b'Jabatan', blank=True),
            preserve_default=True,
        ),
    ]
