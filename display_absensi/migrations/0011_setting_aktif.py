# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0010_auto_20150210_0725'),
    ]

    operations = [
        migrations.AddField(
            model_name='setting',
            name='aktif',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
