# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0013_pegawai_jabatan'),
    ]

    operations = [
        migrations.CreateModel(
            name='FingerprintLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('kategori', models.CharField(max_length=100, verbose_name=b'Kategori Log')),
                ('log', models.TextField(verbose_name=b'Keterangan Log')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Log Fingerprint',
                'verbose_name_plural': 'Log Fingerprint',
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='fingerprint',
            options={'verbose_name': 'Data Absensi', 'verbose_name_plural': 'Data Absensi'},
        ),
    ]
