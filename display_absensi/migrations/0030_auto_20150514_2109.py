# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0029_auto_20150514_1656'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pegawai',
            name='pin',
            field=models.CharField(max_length=16, null=True, verbose_name=b'PIN', blank=True),
            preserve_default=True,
        ),
    ]
