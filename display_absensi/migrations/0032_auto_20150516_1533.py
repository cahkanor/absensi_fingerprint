# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0031_auto_20150515_1002'),
    ]

    operations = [
        migrations.AddField(
            model_name='fingerprinttemplate',
            name='tersinkron',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='pegawai',
            name='tersinkron',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
