# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0015_fingerprintlog_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fingerprint',
            name='pegawai',
            field=models.ForeignKey(to_field=b'id_fingerprint', blank=True, to='display_absensi.Pegawai', null=True),
            preserve_default=True,
        ),
    ]
