# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('display_absensi', '0025_pegawai_pin'),
    ]

    operations = [
        migrations.AddField(
            model_name='pegawai',
            name='privilege',
            field=models.IntegerField(default=0, verbose_name=b'Privilege'),
            preserve_default=True,
        ),
    ]
