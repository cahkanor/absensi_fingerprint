git config --global http.sslverify false

git fetch origin master
git reset --hard FETCH_HEAD
git clean -df

chmod a+x reset_server.sh
chmod a+x restart_server.sh
chmod a+x proxy.sh
chmod a+x unset_proxy.sh
chmod 777 vesion.txt

pip install -r requirement.txt

python manage.py makemigrations
python manage.py migrate
echo yes | python manage.py collectstatic

supervisorctl reread
supervisorctl update
supervisorctl restart all
service nginx restart

