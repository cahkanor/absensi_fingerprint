Django==1.8
django-admin-bootstrapped
django-mptt
django-tastypie
django-debug-toolbar
django-easy-select2
pytz
gunicorn
xlrd
redis
celery
django-celery
drest
babel
requests
pillow
pycrypto
django-bootstrap-pagination
netifaces
kombu
billiard
sqlparse
python-dateutil