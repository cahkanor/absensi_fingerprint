
# export GIT_SSL_NO_VERIFY=1
# sudo git config --global http.sslverify false

sudo git fetch origin master
sudo git reset --hard FETCH_HEAD
sudo git clean -df

sudo chmod a+x reset_server_no_proxy.sh
sudo chmod a+x reset_server.sh
sudo chmod a+x restart_server.sh
sudo chmod a+x proxy.sh
sudo chmod a+x unset_proxy.sh
sudo chmod 777 vesion.txt

sudo pip install -r requirement.txt

sudo python manage.py makemigrations
sudo python manage.py migrate
echo yes | sudo python manage.py collectstatic

# if [ ! -f /home/pi/celery.log ]; then
# 	echo "File /home/pi/celery.log not found!"
# else
# 	sudo rm /home/pi/celery.log
# 	echo "File /home/pi/celery.log deleted!"
# fi

# echo "Restarting server...."

supervisorctl reread
supervisorctl update
supervisorctl restart all
service nginx restart
