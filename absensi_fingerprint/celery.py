from __future__ import absolute_import

import os
from celery import Celery
from django.conf import settings
from celery import task
from celery.task.schedules import crontab
from celery.decorators import periodic_task
from display_absensi.views import crawler_data, get_datalokal, hapus_fingerprint, hapus_data_di_mesin, hapus_log, hapus_attlog, sync_waktu, sinkronisasi_pegawai, sinkronisasi_mesin, sinkronisasi_server
from absensi_fingerprint import settings


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'absensi_fingerprint.settings')

app = Celery('absensi_fingerprint')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.update(
    CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend',
)

# Setiap 2 menit mengambil data di mesin
@periodic_task(run_every=(crontab(minute='*/1')))
def task_crawling_fingerprint():
	return crawler_data()

# Setiap 4 menit mengirim data absensi yang ada di table fingerprint yang belum terkirim ke server / sinyal ke server
@periodic_task(run_every=(crontab(minute='*/3')))
def task_submit_fingerprint():
	return get_datalokal()

# Setiap siang dan malam sinkronisasi waktu 
@periodic_task(run_every=(crontab(minute=0, hour='5,6,9,10,12,13,15,18,19')))
def task_syncron_waktu():
	return sync_waktu()
	
# Setiap Jam 6 menghapus fingerprint yang sudah terkirim
@periodic_task(run_every=(crontab(hour='4,19', minute='0')))
def task_hapus_fingerprint():
	return hapus_fingerprint()

# Setiap 3 jam menghapus att log 
@periodic_task(run_every=(crontab(minute=0, hour='3,10,12,18,21')))
def task_hapus_attlog():
	return hapus_attlog()

# Setiap 20 menit setelah check waktu
@periodic_task(run_every=(crontab(minute=20, hour='5,18')))
def task_hapus_log():
	return hapus_log()

# Setiap setengah 6 dan setengah 12 siang
@periodic_task(run_every=(crontab(minute=10, hour='5,10,15,18')))
def task_syncron_pegawai():
	return sinkronisasi_pegawai()

# Setiap siang dan malam sinkronisasi waktu 
# @periodic_task(run_every=(crontab(minute=0, hour='5,10')))
# def task_syncron_mesin():
# 	return sinkronisasi_mesin()

# Setiap siang dan malam sinkronisasi waktu 
# @periodic_task(run_every=(crontab(minute=35, hour='5,10')))
# def task_syncron_server():
# 	return sinkronisasi_server()


#@periodic_task(run_every=(crontab(hour='14',minute='0')))
# def task_hapus_data_di_mesin():
# 	return hapus_data_di_mesin()

#@periodic_task(run_every=(crontab(minute=settings.MENIT_KIRIM_DATA, hour=settings.JAM_KIRIM_DATA)))
#@periodic_task(run_every=(crontab(minute=settings.MENIT_KIRIM_DATA, hour='*/1')))
#@periodic_task(run_every=(crontab(hour='*/1',minute='0')))