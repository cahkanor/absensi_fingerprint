from django.conf.urls import patterns, include, url
from django.contrib import admin
from display_absensi import views
import settings

urlpatterns = patterns('',
    # Examples:
    url(r'^$', views.display, name='home'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^display/', views.display, name='display'),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', 
            {'document_root': settings.STATIC_ROOT, 
            'show_indexes': True}),
)
